# TechMagic Test django app

Test bookstore application with using Django.

## Running an application

### Configuration before start

Create an `.env` file with next settings:

```console
DJANGO_SECRET_KEY=super-secret-key
POSTGRES_USER=tech_magic_user
POSTGRES_PASSWORD=tech_magic_password
POSTGRES_DB=tech_magic_db
POSTGRES_HOST=db
POSTGRES_PORT=5432
```

### Start container

Run docker compose:

```console
docker compose up -d
```

Apply migrations and load fixtures with books on the container first start:

```console
docker exec -it techmagicbook-web-1 /code/docker-entrypoint.sh
```

Create a Django superuser to check the admin site:

```console
docker exec -it techmagicbook-web-1 python manage.py createsuperuser
```

### Useful links:

- [localhos:8000/admin/](http://localhost:8000/admin/) - Admin site
- [localhos:8000/books/](http://localhost:8000/books/) - Books DRF page
- [localhos:8000/orders/](http://localhost:8000/orders/) - Orders DRF page
- [localhos:8000/orders/1](http://localhost:8000/orders/1) - Use HTTP DELETE 
   with Order <pk> to delete 