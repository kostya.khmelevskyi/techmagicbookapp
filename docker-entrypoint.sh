#!/bin/bash

# Apply database migrations
echo "Apply database migrations"
python manage.py migrate

# Load database fixtures
echo "Load database fixtures"
python manage.py loaddata store/fixtures/Author.json
python manage.py loaddata store/fixtures/Book.json
