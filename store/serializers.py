from rest_framework import serializers

from store.models import Book, Order, BookOrder


class BookSerializer(serializers.ModelSerializer):
    author = serializers.CharField(source='author.name')

    class Meta:
        model = Book
        fields = ['id', 'name', 'author', 'price', 'count', ]


class BookOrderSerializer(serializers.ModelSerializer):
    book_id = serializers.IntegerField(source='book.id')
    price = serializers.DecimalField(max_digits=10, decimal_places=2, read_only=True)

    class Meta:
        model = BookOrder
        fields = ['book_id', 'quantity', 'price', ]


class OrderSerializer(serializers.ModelSerializer):
    books = BookOrderSerializer(source='order_books', many=True)

    class Meta:
        model = Order
        fields = ['id', 'books', ]

    @staticmethod
    def validate_books(value):
        if not value:
            raise serializers.ValidationError('Provide at least one book for order')
        return value
