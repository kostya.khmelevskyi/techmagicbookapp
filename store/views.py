from rest_framework import permissions, generics, status
from rest_framework.response import Response
from rest_framework.serializers import ValidationError

from store.models import Book, Order
from store.serializers import BookSerializer, OrderSerializer


class BookView(generics.ListAPIView):
    queryset = Book.objects.order_by('id')
    serializer_class = BookSerializer
    permission_classes = [permissions.AllowAny, ]


class OrderView(generics.ListCreateAPIView):
    queryset = Order.objects.order_by('id')
    serializer_class = OrderSerializer
    permission_classes = [permissions.AllowAny, ]

    def post(self, request, *args, **kwargs):
        order_books_serializer = self.serializer_class(data=request.data)
        order_books_serializer.is_valid(raise_exception=True)
        try:
            order = Order.create_order(
                books=order_books_serializer.validated_data.get('order_books'))
        except Book.DoesNotExist as _:
            raise ValidationError('Please check books ID, '
                                  'there is no books with provided ID')
        except ValueError as _:
            raise ValidationError('There is not enough books in stock to '
                                  'complete your order. Please check available '
                                  'quantity and try again.')
        except Exception as e:
            # log the exception
            return Response('Unable to create an Order.',
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        serializer = self.serializer_class(instance=order, many=False)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        order = self.get_object()
        order.delete_order()
        return Response(status=status.HTTP_204_NO_CONTENT)
