from typing import List
from decimal import Decimal

from django.db import models, transaction
from django.db.models import Sum, F
from django.utils.translation import ugettext_lazy as _


class Author(models.Model):
    name = models.CharField(max_length=500)

    class Meta:
        verbose_name = _('Author')
        verbose_name_plural = _('Authors')

    def __str__(self):
        return f'Author ID {self.id} | {self.name}'


class Book(models.Model):
    name = models.CharField(max_length=500)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    count = models.PositiveIntegerField(db_index=True)
    author = models.ForeignKey(to='Author', related_name='books', on_delete=models.PROTECT)

    class Meta:
        verbose_name = _('Book')
        verbose_name_plural = _('Books')

    def __str__(self):
        return f'Book ID {self.id} | {self.name}'


class Order(models.Model):
    # Fields about order details like when it was created and
    # what user made this ordered

    class Meta:
        verbose_name = _('Order')
        verbose_name_plural = _('Orders')

    def __str__(self):
        return f'Order ID {self.id}'

    @property
    def books(self):
        return self.order_books.all()

    @property
    def total_price(self) -> Decimal:
        return self.books \
            .annotate(total_price_per_book=F('price') * F('quantity')) \
            .aggregate(total_price=Sum('total_price_per_book'))['total_price']

    @property
    def total_books(self) -> int:
        return self.books \
            .aggregate(total_books_qty=Sum('quantity'))['total_books_qty']

    @classmethod
    def create_order(cls, books: List):
        books_for_order = {}
        for book in books:
            book_id = book['book']['id']
            book_quantity = book['quantity']
            if book_id in books_for_order:
                books_for_order[book_id] += book_quantity
            else:
                books_for_order[book_id] = book_quantity
        books_objects = Book.objects.select_for_update() \
            .filter(id__in=books_for_order.keys())
        with transaction.atomic():
            if books_objects.count() != len(books_for_order.keys()):
                raise Book.DoesNotExist
            # do not create an Order if any quantity is not enough
            for book in books_objects:
                if book.count < books_for_order[book.id]:
                    raise ValueError('Book is out of stock')
            order = cls.objects.create()
            for book in books_objects:
                _ = BookOrder.create_book_order(order=order, book=book,
                                                quantity=books_for_order[book.id])
        return order

    def delete_order(self) -> None:
        for book_order in self.order_books.all():
            book_order.delete_book_order()
        if not self.order_books.exists():
            self.delete()


class BookOrder(models.Model):
    order = models.ForeignKey(to='Order', related_name='order_books', on_delete=models.PROTECT)
    book = models.ForeignKey(to='Book', related_name='book_orders', on_delete=models.PROTECT)
    quantity = models.PositiveIntegerField()
    price = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        verbose_name = _('Book')
        verbose_name_plural = _('Books')

    def __str__(self):
        return f'Book ID {self.id}'

    @classmethod
    def create_book_order(cls, order: Order, book: Book, quantity: int):
        with transaction.atomic():
            book_order = cls.objects.create(
                order=order,
                book=book,
                quantity=quantity,
                price=book.price
            )
            book.count -= quantity
            book.save()
        return book_order

    def delete_book_order(self) -> None:
        book_to_update = self.book
        book_to_update.count += self.quantity
        book_to_update.save()
        self.delete()
