from decimal import Decimal

from django.contrib import admin
from django.utils.html import format_html_join
from django.utils.safestring import mark_safe

from store.models import Author, Book, Order, BookOrder


class AuthorAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


class BookAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'author', 'price', 'count']


class BookOrderInline(admin.TabularInline):
    model = BookOrder

    class Meta:
        abstract = True

    def has_change_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'books', 'total_price', 'total_books', ]
    inlines = [BookOrderInline]

    @admin.display(description='Books')
    def books(self, order: Order) -> str:
        return format_html_join(
            mark_safe('<br>'), '{} | ${} | Qty. {}',
            ((str(book_order.book), book_order.price, book_order.quantity,)
             for book_order in order.order_books.order_by('id'))
        )

    @admin.display(description='Total Price')
    def total_price(self, order: Order) -> Decimal:
        return order.total_price

    @admin.display(description='Total Books Qty.')
    def total_books(self, order: Order) -> int:
        return order.total_books


admin.site.register(Author, AuthorAdmin)
admin.site.register(Book, BookAdmin)
admin.site.register(Order, OrderAdmin)
