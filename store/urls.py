from django.urls import path

from store.views import BookView, OrderView

urlpatterns = [
    path('books/', BookView.as_view(), name='books'),
    path('orders/', OrderView.as_view(), name='orders'),
    path('orders/<int:pk>', OrderView.as_view(), name='order_delete'),
]
